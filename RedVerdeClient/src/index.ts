import { Element, ElementImage, LoginInfo, Marker, MarkerDetail, SearchResults, User, LoginResp } from './dtos';
import { HTTPMethod, RestClient } from './lib/rest-client';
import { delay } from './lib/utils';
import sha1 = require('sha1');

class GenericResponse<T>{
    constructor(
        public code:number,
        public response:T
    ){}
}

interface MarkersResponse {
    markers: Marker[];
}

async function testEmailRegister() {
    let client = new RestClient("http://localhost:8080/", false);
    let user:User = {
        nombre: "Charles Xavier",
        correo: "charlesx@xmen.com",
        password: "m16l4Ve53Cr3t@",
        tipoUsuario: "C"
    }
    let r1 = await client.callMethod('/register', HTTPMethod.POST, user);
    console.log(`result: ${JSON.stringify(r1.obj)}`);
}

async function testSocialRegister() {
    let client = new RestClient("http://localhost:8080/", false);
    let fbId = '1516712398352566';
    let pwd = sha1(`yZc]#"/E}&pB,${fbId}rDxyTd )m'Fg`).toString();
    let user:User = {
        nombre: "Charles Xavier",
        correo: "FB::charlesx@xmen.com",
        fbId: fbId,
        password: pwd,
        tipoUsuario: "C"
    }
    let r1 = await client.callMethod('/register', HTTPMethod.POST, user);
    console.log(`result: ${JSON.stringify(r1.obj)} pwd: ${pwd}`);
}

async function testEmailLogin() {
    let client = new RestClient("http://localhost:8080/", false);
    let r1 = await client.callMethod('/login', HTTPMethod.POST, <LoginInfo>{
        correo: "charlesx@xmen.com",
        password: "m16l4Ve53Cr3t@"
    });
    console.log(`result: ${JSON.stringify(r1.obj)}`);
}

async function testSocialLogin() {
    let client = new RestClient("http://localhost:8080/", false);
    let fbId = '1516712398352566';
    let pwd = sha1(`yZc]#"/E}&pB,${fbId}rDxyTd )m'Fg`).toString();
    let r1 = await client.callMethod('/login', HTTPMethod.POST, <LoginInfo>{
        correo: "FB::charlesx@xmen.com",
        password: pwd
    });
    console.log(`result: ${JSON.stringify(r1.obj)}`);
}

async function testUser() {
    let client = new RestClient("http://localhost:8080/", false);
    let user:User = {
        nombre: "Charles Xavier",
        correo: "charlesx@xmen.com",
        password: "654321",
        sexo: "M",
        tipoUsuario: "C"
    }
    try {
        //await client.callMethod("/register", HTTPMethod.POST, user);
    } catch(error) {
        console.log(error.message);
    }
    
    //await delay(5);
    //await client.callMethod("/confirm/3c61fbc8ed0d23d1736f5b9abadb4f80eb5d5bc1");
    let authResp = await client.callMethod<any>("/login", HTTPMethod.POST, <LoginInfo>{
        correo:"charlesx@xmen.com",
        password: "m16l4Ve53Cr3t@"
    });
    client.setReqHeader("auth-token", authResp.obj.token);
    let userResp = await client.callMethod<User>("/profile");
    let storedUser = userResp.obj;
    //console.dir(userResp.obj);
    storedUser.telefono = "33-44-55-66-77";
    //await client.callMethod<User>("/updateProfile", HTTPMethod.POST, storedUser);
    userResp = await client.callMethod<User>("/profile");
    console.dir(userResp.obj);
    //await client.callMethod<any>("/login", HTTPMethod.DEL)
    //userResp = await client.callMethod<User>("/profile");
}

async function testElements() {
    let client = new RestClient("http://localhost:8080/", true);
    let loginResp = await client.callMethod<any>("/login", HTTPMethod.POST, <LoginInfo>{"correo":"usuario3@server.com", "password":"78927892"});
    client.setReqHeader("auth-token", loginResp.obj.token);
    let element:Element = {
        nombre: "Lote 1",
        tipoPropiedadId: 1,
        lat: 20.675363,
        lng: -103.425928
    }
    let r1 = await client.callMethod<Element>("/element", HTTPMethod.POST, element);
    let r2 = await client.callMethod<MarkersResponse>(`/getMarkers?startLat=${20.0}&startLong=${-104.0}&endLat=${21.0}&endLong=${-103}&types=${1|2|4|8}`);
    let markers = r2.obj.markers;
    let r3 = await client.callMethod<Element>(`/element/${markers[0].elementId}`);
    let elementInfo = r3.obj;
    elementInfo.descripcion = "Detalle del lote";
    //await client.callMethod(`/element/${elementInfo.Id}`, HTTPMethod.PUT, elementInfo);
    let r4 = await client.callMethod<Element>(`/element/${markers[0].elementId}`);
    console.dir(r4);
    //await client.callMethod(`/element/${elementInfo.Id}`, HTTPMethod.DEL);
}

async function  testMarkers() {
    let client = new RestClient("http://localhost:8080/", true);
    let r = await client.callMethod<MarkersResponse>(`/getMarkers?startLat=${20.0}&startLong=${-104.0}&endLat=${21.0}&endLong=${-103}&types=${1|2|4|8}`);
    let data = JSON.stringify(r.obj,null,4);
    console.log(data);
}

async function  testSearch() {
    let key = "zapopan";
    key = encodeURIComponent(key);
    let client = new RestClient(`http://localhost:8080/`, false);
    let r = await client.callMethod<SearchResults>(`/search/${key}`);
    let data = JSON.stringify(r.obj.elements,null,4);
    console.log(data);
}

async function testGetMarkDetail() {
    let client = new RestClient(`http://localhost:8080/`, false);
    let r = await client.callMethod<MarkerDetail>(`/getMarkDetail/${522}`);
    let data = JSON.stringify(r.obj,null,4);
    console.log(data);
}

async function testNetworking() {
    let client = new RestClient(`http://localhost:8080/`, false);
    let loginResp = await client.callMethod<any>("/login", HTTPMethod.POST, <LoginInfo>{"correo":"usuario3@server.com", "password":"78927892"});
    client.setReqHeader("auth-token", loginResp.obj.token);
    let r1 = await client.callMethod<MarkerDetail>(`/network/admin?types=${1|2|4|8}&searchKey=zapotlanejo`);
    let data1 = JSON.stringify(r1.obj,null,4);
    let r2 = await client.callMethod<MarkerDetail>(`/network/participant?types=${1|2|4|8}`);
    let data2 = JSON.stringify(r2.obj,null,4);
    console.log(data2);
}

async function testNetworkingManagement(){
    let client = new RestClient("http://localhost:8080/", false);
    let loginInfo:LoginInfo = { correo: "usuario3@server.com", password: "78927892" };
    let loginResp = await client.callMethod<any>("/login", HTTPMethod.POST, loginInfo);
    client.setReqHeader("auth-token", (loginResp.obj as LoginResp).token);
    //await client.callMethod<GenericResponse<boolean>>(`/network/802`, HTTPMethod.POST);
    //await client.callMethod<GenericResponse<boolean>>(`/network/802`, HTTPMethod.DEL);
}

async function testImages() {
    let client = new RestClient(`http://localhost:8080/`, true);
    let loginResp = await client.callMethod<any>("/login", HTTPMethod.POST, <LoginInfo>{"correo":"usuario3@server.com", "password":"78927892"});
    client.setReqHeader("auth-token", loginResp.obj.token);
    let r1 = await client.callMethod<ElementImage>("/images/280");
    let data1 = JSON.stringify(r1.obj,null,4);
    console.log(data1);
    let r2 = await client.callMethod<GenericResponse<number>>("/image", HTTPMethod.POST, <ElementImage>{
        elemementId: 280,
        imagePath: "https://redverde.com.mx/images/201709/148/img_20170905041303_normal.png"
    });
    let data2 = JSON.stringify(r2.obj, null, 4);
    console.log(data2);
    let r3 = await client.callMethod<any>(`/image/1`, HTTPMethod.DEL);
    let data3 = JSON.stringify(r3.obj, null, 4);
    console.log(data3);
}

async function main(){
    try {
        //await testSocialRegister();
        //await testSocialLogin();
        //await testEmailRegister();
        //await testEmailLogin();
        //await testUser();
        //await testElements();
        //await testMarkers();
        //await testSearch();
        //await testGetMarkDetail();
        //await testNetworking();
        //await testImages();
        //await testNetworkingManagement();
    } catch(error) {
        console.dir(error);
    }
}
main();