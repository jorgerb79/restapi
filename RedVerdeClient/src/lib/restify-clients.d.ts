
declare module 'restify-clients' {

    import { Request, Response } from "restify";

    export type ClientOptions = { [header:string]:any };

    export type HttpOptions = { [header:string]:any };

    export function createJsonClient(opt:ClientOptions):JsonClient;

    export interface JsonClient  {

        get(opt:HttpOptions, handler:any): void;

        post(opt:HttpOptions, handler:any): void;
        post(opt:HttpOptions, data:any, handler:any): void;

        put(opt:HttpOptions, handler:any): void;
        put(opt:HttpOptions, data:any, handler:any): void;

        del(opt:HttpOptions, handler:any): void;
    }

}