/// <reference path="./restify-clients.d.ts" />
import restifyClients = require('restify-clients');
import restify = require('restify');

function objExtend(...obj:any[]){
    if(obj.length==0) return;
    let newObj = obj.shift();
    obj.forEach(o=>{
        for(let k in obj){
            newObj[k] = obj[k];
        }
    });
    return newObj;
}

export enum HTTPMethod {
    GET,
    POST,
    DEL,
    PUT
}

export class RestError extends Error {
    public httpCode:number
    public code:number;

    constructor(httpCode:number, code:number, message?:any){
        super(message);
        this.httpCode = httpCode;
        this.code = code;
    }
}

export interface Response extends restify.Response {
    body: string|Buffer
}

export interface Request extends restify.Request {
}

export interface RestClientResponse<T> {
    req:restify.Request,
    res:restify.Response,
    obj:T
}

export class RestClient {
    private restClient:restifyClients.JsonClient;
    
    constructor(
        public baseUrl:string,
        public debug:boolean = false,
        private headers:any = {}
    ){
        this.baseUrl = this.baseUrl.replace(/\/?\s*$/,"");
        this.restClient = restifyClients.createJsonClient({
            url: baseUrl,
            version: "*"
        });
    }

    setReqHeader(header:string, value:string){
        this.headers[header] = value; 
    }

    delReqHeader(header:string){
        delete this.headers[header];
    }

    callMethod<T>(path:string, method:HTTPMethod=HTTPMethod.GET, data:any=null): Promise<RestClientResponse<T>> {
        return new Promise<RestClientResponse<T>>( (resolve, reject)=>{
            let reqId = Date.now();
            let fullPath = `${this.baseUrl}${path}`;
            if(this.debug){
                console.log(`
                    @---------------------- REQUEST ----------------------
                    @ ID: ${reqId}
                    @ PATH: ${fullPath}
                    @ METHOD: ${HTTPMethod[method]}
                    @ EXTRA-HEADERS: ${JSON.stringify(this.headers)}
                    @------------------------------------------------------
                `.replace(/^.*?@/mg,"").trim());
            }
            let handler = (err:any, req:Request, res:Response, obj:any = null) => {
                if(this.debug){
                    console.log(`
                        @---------------------- RESPONSE ----------------------
                        @ ID: ${reqId}
                        @ HEADERS: ${res?JSON.stringify(res.headers):"{}"}
                        @ STATUS-CODE: ${res?JSON.stringify(res.statusCode):"{}"}
                        @ ERROR: ${err?JSON.stringify(err):"null"}
                        @ BODY: ${res?res.body:{}}
                        @------------------------------------------------------
                    `.replace(/^.*?@/mg,"").trim());
                }
                if(err){
                    reject(err);
                } else {
                    resolve( <RestClientResponse<T>>{
                        req:req,
                        res:res,
                        obj:obj as T
                    });
                }
            }
            let opt:{ path?: string; [name: string]: any } = { path:path, headers:this.headers };
            switch(method){
                case HTTPMethod.GET:
                    this.restClient.get(opt, handler);
                    break;
                case HTTPMethod.POST:
                    this.restClient.post(opt, data, handler);
                    break;
                case HTTPMethod.PUT:
                    this.restClient.put(opt, data, handler);
                    break;
                case HTTPMethod.DEL:
                    this.restClient.del(opt, handler);
            }
        });
    }
}