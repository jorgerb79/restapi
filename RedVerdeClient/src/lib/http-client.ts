
import fnRequest = require('request');
import fs = require('fs');

export interface HttpRequestOptions {
    content?: any;
    dontFollowRedirects?: string;
    headers?:any;
    method?:string;
    timeout?:number;
    url:string;
    json?:boolean;
}

export class File {
    constructor(
        public filename:string,
        public data:Buffer|string
    ){}
}

export interface Response extends fnRequest.RequestResponse {}

export function request(options:HttpRequestOptions):Promise<Response>{
    return new Promise((resolve,reject)=>{
        let opt = options;
        let reqOpt = <fnRequest.CoreOptions>{
            followAllRedirects: !opt.dontFollowRedirects,
            timeout: opt.timeout != undefined ? opt.timeout : 120000,
            headers: opt.headers != undefined ? opt.headers : {},
            json: opt.json,
            body: opt.content,
            method: opt.method
        };
        fnRequest(opt.url, reqOpt, (err, resp, body)=>{
            if(err!=undefined){
                reject(err);
            } else {
                if(resp.statusCode==200){
                    resolve(resp.body);
                } else {
                    reject(resp);
                }
                
            }
        });
    });
}

export function getString(urlOrOpt:string|HttpRequestOptions):Promise<string> {
    let opt:HttpRequestOptions;
    if(typeof(urlOrOpt) === 'string'){
        opt = { url: urlOrOpt };
    } else {
        opt = urlOrOpt;
    }
    return new Promise((resolve,reject)=>{
        request(opt).then(r=>resolve(r.body)).catch(e=>reject(e));
    });
}

export function getJSON<T>(urlOrOpt:string|HttpRequestOptions):Promise<T> {
    let opt:HttpRequestOptions;
    if(typeof(urlOrOpt) === 'string'){
        opt = { url: urlOrOpt };
    } else {
        opt = urlOrOpt;
    }
    opt.json = true;
    return new Promise((resolve,reject)=>{
        request(opt).then(r=>{
            if(r.statusCode==200){
                resolve(r.body as T)
            } else {
                reject(r);
            }
        }).catch(e=>reject(e));
    });
}


export function getImage(urlOrOpt:string|HttpRequestOptions):Promise<string> {
    let opt:HttpRequestOptions;
    if(typeof(urlOrOpt) === 'string'){
        opt = { url: urlOrOpt };
    } else {
        opt = urlOrOpt;
    }
    return new Promise((resolve,reject)=>{
        request(opt).then(r=>{
            if(r.statusCode==200){
                resolve(r.body as string)
            } else {
                reject(r);
            }
        }).catch(e=>reject(e));
    });
}

export function getFile(urlOrOpt:string|HttpRequestOptions, destPath?:string):Promise<File> {
    let opt:HttpRequestOptions;
    if(typeof(urlOrOpt) === 'string'){
        opt = { url: urlOrOpt };
    } else {
        opt = urlOrOpt;
    }
    return new Promise((resolve,reject)=>{
        request(opt).then(r=>{
                if(r.statusCode==200){
                    if(destPath == undefined){
                    destPath = `tmp-${Date.now()}`
                }
                fs.writeFileSync(destPath, r.body);
                let f = new File(destPath, r.body);
                resolve(f)
            } else {
                reject(r);
            }
        }).catch(e=>reject(e));
    });
}

