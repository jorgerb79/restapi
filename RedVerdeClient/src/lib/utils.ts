import http = require('./http-client');

export function extend(...obj:any[]){
    if( obj.length==0 ) return;
    let result:any = obj.shift();
    obj.forEach(obj=>{
        for(let k in obj){
            result[k] = obj[k];
        }
    });
    return result;
}


export class Req implements http.HttpRequestOptions {
    content?: string;
    dontFollowRedirects?: string;
    headers?:any;
    method?:string;
    timeout?:number;
    url:string;
    json?:boolean;

    constructor(url:string, extraHeaders:any){
        this.url = url;
        let headers = extend({}, extraHeaders);
        extend(this, <http.HttpRequestOptions>{
            url: url,
            headers: headers
        });

    }
}


export function delay(seconds:number){
    return new Promise(resolve=>{
        setTimeout(()=>{
            resolve();
        }, seconds*1000);
    });
}