import { Element } from '../shared/dtos';
import { getSearchKeyFilter, getTypesFilter } from '../lib/utils';
import { DBConnectionPool } from '../shared/db-connector';

import { RestError } from '../lib/ServiceBase';
import { ClientError, ServerError } from '../shared/error-codes';

export class NetworkingAPI {
    constructor(
        private db:DBConnectionPool
    ){}

    async addToMyNetwork(userId:number, elementId:number):Promise<void> {
        let query = `
            Insert into Network (userId, elementId) values (?,?);
        `;
        try {
            let result = await this.db.run(query, userId, elementId);
        } catch (error) {
            if(error.message && error.message.toLowerCase().indexOf('duplicate') ){
                let e = ServerError.DBDuplicatedValue;
                throw new RestError(e.httpError, e.code, `Element ${elementId} is already in your network`);
            } else {
                throw error;
            }
        }
    }

    async removeFromNetwork(userId:number, elementId:number):Promise<void> {
        let query = `
            delete from Network where userId = ? and elementId = ?
        `;
        try {
            let result = await this.db.run(query, userId, elementId);
            if(result.affectedRows==0){
                let e = ServerError.DBNoRecord;
                throw new RestError(e.httpError, e.code, `Element with Id ${elementId} not found in user's network`);
            }
        } catch (error) {
            throw error;
        }
    }

    async getMyNetworkParticipant(userId:number, types:number, searchKey:string=''):Promise<Element[]> {
        let typesFilter = getTypesFilter(types);
        searchKey = getSearchKeyFilter(searchKey, true);
        let searchFields = `concat_ws(' ', coalesce(e.nombre,''), coalesce(e.domicilio,''))`;
        let query = `
            select
                e.Id as elementId, 
                e.nombre,
                e.domicilio,
                u.nombre as administrador,
                e.fotoFolder,
                n.userId,
                e.administradorId,
                e.points
            from Elements e
            join Users u on u.Id = e.administradorId
            join Network n on n.elementId = e.Id
            where
                (n.userId = ? and e.administradorId <> ?)
                and ${typesFilter}
                ${searchKey.length>0?`and ${searchFields} like ?`:''}
            order by e.nombre, e.domicilio
        `;
        let result = await this.db.all(query, userId, userId, searchKey);
        return result;
    }
    
    async getMyNetworkAdmin(userId:number, types:number, searchKey:string=''):Promise<Element[]> {
        let typesFilter = getTypesFilter(types);
        searchKey = getSearchKeyFilter(searchKey, true);
        let searchFields = `concat_ws(' ', coalesce(e.nombre,''), coalesce(e.domicilio,''))`;
        let query = `
            select
                e.Id as elementId, 
                e.nombre,
                e.domicilio,
                u.nombre as administrador,
                e.fotoFolder,
                e.administradorId,
                e.points
            from Elements e
            join Users u on u.Id = e.administradorId
            where
                e.administradorId = ?
                and ${typesFilter}
                ${searchKey.length>0?`and ${searchFields} like ?`:''}
            order by e.nombre, e.domicilio
        `;
        let result = await this.db.all(query, userId, searchKey);
        return result;
    }
}
