import { getSearchKeyFilter, getTypesFilter } from '../lib/utils';
import { DBConnectionPool } from '../shared/db-connector';
import { isAlpha } from 'validator';

import { RestError } from '../lib/ServiceBase';
import { Element, Marker, MarkerDetail, SearchResult, ElementImage } from '../shared/dtos';
import { ClientError, ServerError } from '../shared/error-codes';

export class ElementAPI {
    constructor(
        private db:DBConnectionPool
    ){}

    async createElement(element:Element):Promise<number>{
        element.creationDate = (new Date()).toISOString();
        let el = element as any;
        let fields:string[] = [];
        let values:any[] = [];
        for(let f in el){
            if(!isAlpha(f)){ 
                let e = ClientError.InvalidParamName;
                throw new RestError(e.httpError, e.code, `Invalid field ${f}`);
            }
            fields.push(f);
            values.push( el[f] ); 
        }
        let strFields = fields.join(",");
        let strSlots = fields.map(f=>"?").join(",");
        let insertQuery = `Insert into Elements (${strFields}) values (${strSlots})`;
        let resp = await this.db.run(insertQuery, ...values);
        if(resp.affectedRows==0){
            let e = ServerError.DBErrorQuery;
            throw new RestError(e.httpError, e.code, "Unable to create User");
        }
        return resp.insertId;
    }

    async updateElement(elementId:number, element:Element):Promise<void>{
        let el = element as any;
        if(element['Id']) delete element['Id'];
        if( el['nombre'] ) delete el['nombre'];
        let fields:string[] = [];
        let values:any[] = [];
        for(let f in el){
            if(!isAlpha(f)){ 
                let e = ClientError.InvalidParamName;
                throw new RestError(e.httpError, e.code, `Invalid field ${f}`);
            }
            fields.push(f);
            values.push( el[f] ); 
        }
        let strFields = fields.join(",");
        let strSlots = fields.map(f=>`${f} = ?`).join(",");
        let updateQuery = `Update Elements set ${strSlots} where Id = ?`;
        values.push(elementId);
        let res = await this.db.run(updateQuery, ...values);
        if(res.affectedRows==0){
            let e = ServerError.DBNoRecord;
            throw new RestError(e.httpError, e.code, `Couldn't find an element with Id: ${elementId}`);
        }
    }

    async updateElementField(elementId:number, field:string, value:string|number){
        if( !isAlpha(field) ){
            let e = ClientError.InvalidParamName;
            throw new RestError(e.httpError, e.code, `Invalide field ${field}`);
        }
        let updateQuery = `Update Elements set ${field} = ? where Id = ?`;
        let res = await this.db.run(updateQuery, value, elementId);
        if(res.affectedRows==0){
            let e = ServerError.DBNoRecord;
            throw new RestError(e.httpError, e.code, `Couldn't find an element with Id: ${elementId}`);
        }
    }

    async deleteElement(elementId:number){
        let updateQuery = `Delete from Elements where Id = ?`;
        let res = await this.db.run(updateQuery, elementId);
        if(res.affectedRows==0){
            let e = ServerError.DBNoRecord;
            throw new RestError(e.httpError, e.code, `Couldn't find an element with Id: ${elementId}`);
        }
    }

    async getElement(elementId:number):Promise<Element>{
        let res = await this.db.get<any>(`
            Select E.*, U.nombre
            from Elements E 
            join Users U on E.administradorId = U.Id
            where E.Id = ?
        `, elementId);
        if(!res){
            let e = ServerError.DBNoRecord;
            throw new RestError(e.httpError, e.code, `Couldn't find an element with Id: ${elementId}`);
        }
        return res;
    }

    async getMarkers(startLat:number, startLong:number, endLat:number, endLong:number, types:number):Promise<Marker[]>{
        let typesFilter = getTypesFilter(types);
        let seletQuery = `
            Select Id as elementId, lat, lng, tipoPropiedadId from Elements
            where (lat between ? and ?) and (lng between ? and ?) and ${typesFilter}
            order by creationDate desc
            limit 50`;
        let res = await this.db.all<any>(seletQuery, startLat, endLat, startLong, endLong);
        return res;
    }

    async registerImage(elementId:number, path:string) {
        let sql = `
            INSERT INTO Images (elementId, imagePath, creationDate)
            VALUES (?, ?, ?)
        `;
        let creationDate = (new Date()).toISOString();
        let resp = await this.db.run<any>(sql, elementId, path, creationDate);
        if(resp.affectedRows==0){
            let e = ServerError.DBErrorQuery;
            throw new RestError(e.httpError, e.code, "Unable to register Image");
        }
        return resp.insertId;
    }

    async removeImage(imageId:number) {
        let sql = `DELETE FROM Images WHERE Id = ?`;
        let resp = await this.db.run<any>(sql, imageId);
        if(resp.affectedRows==0){
            let e = ServerError.DBErrorQuery;
            throw new RestError(e.httpError, e.code, "Couldn't find the corresponding elementId or imagePath");
        }
    }

    async getImages(elementId:number):Promise<ElementImage[]>{
        let sql = `SELECT * FROM Images WHERE ElementId = ?`;
        let resp = await this.db.all<ElementImage>(sql, elementId);
        return resp;
    }

    async getMarkDetail(elementId:number, userId:number):Promise<MarkerDetail>{
        let res = await this.db.get<Element>(`
        Select 
            N.elementId, E.*, U.nombre as administrador,
            (case when N.elementId is not null then 1 else 0 end) as isAdded
        from Elements E 
        join Users U on E.administradorId = U.Id
        left join Network N on N.userId = ? and N.elementId = E.Id
        where E.Id = ?
        `,  userId, elementId);
        if(!res || typeof(res.Id) == 'undefined'){
            let e = ServerError.DBNoRecord;
            throw new RestError(e.httpError, e.code, `Couldn't find an element with Id: ${elementId}`);
        }
        let result:MarkerDetail = {
            elementId: res.Id,
            nombre: res.nombre,
            domicilio: res.domicilio || "",
            lat: res.lat,
            lng: res.lng,
            points: res.points,
            isAdmin: res.administradorId == userId,
            isAdded: res.isAdded == 1
        };
        return result;
    }

    async search(searchKey:string):Promise<SearchResult[]>{
        searchKey = getSearchKeyFilter(searchKey);
        let selectQuery = `
            Select 
                Id as elementId,
                tipoPropiedadId,
                lat,
                lng,
                nombre,
                fotoFolder,
                domicilio,
                points
            from Elements
            where concat_ws(' ', coalesce(nombre,''), coalesce(domicilio,'')) like ?
            order by creationDate desc
            limit 11
        `;
        let res = await this.db.all<SearchResult>(selectQuery, searchKey);
        return res;
    }
}
