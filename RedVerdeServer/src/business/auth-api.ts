import { DBConnectionPool } from '../shared/db-connector';
import sha1 = require('sha1');

import { RestError } from '../lib/ServiceBase';
import { LoginResp } from '../shared/dtos';
import { ClientError, ServerError } from '../shared/error-codes';

export class AuthAPI {
    tokenValidMS = 10*365*24*3600*1000;

    constructor(
        private db:DBConnectionPool
    ){}

    async login(email:string, password:string):Promise<LoginResp> {
        let userSql = "select * from Users where correo = ? and password = ?";
        let userInfo = await this.db.get<any>(userSql, email, password);
        if( !userInfo ){
            let e = ClientError.InvalidUserPassword;
            throw new RestError(e.httpError, e.code, "Invalid User/Password");
        }
        if(!userInfo.confirmed){
            let e = ClientError.UnConfirmedEmail;
            throw new RestError(e.httpError, e.code, "Email is not confirmed");
        }
        let timestamp = Date.now();
        let exp = timestamp + this.tokenValidMS;
        let expDate = (new Date(exp)).toISOString();
        let tokenString = `${timestamp}-RedVerdeEsLaOnda-User-${userInfo.usuario}-Id-${userInfo.Id}-AppRedVerde`;
        let token = sha1(tokenString).toString();
        let cleanSql = "delete from Tokens where userId = ?";
        await this.db.run(cleanSql, userInfo.Id);
        let insertSql = "insert into Tokens (userId, token, expDate) values (?,?,?)";
        let res = await this.db.run(insertSql, userInfo.Id, token, expDate);
        return <LoginResp>{ token: token, name: userInfo.nombre, urlImagesBase:"http://localhost" };
    }

    async getUserId(token:string):Promise<number>{
        let currentDate = (new Date()).toISOString();
        await this.db.run("delete from Tokens where expDate <= ?", currentDate);
        let tokenInfo = await this.db.get<any>("select * from Tokens where token = ?", token);
        if( tokenInfo == null ) {
            let e = ClientError.InvalidToken;
            throw new RestError(e.httpError, e.code, "Invalid Auth Token")
        }
        let exp = Date.now() + this.tokenValidMS;
        let expDate = (new Date(exp)).toISOString();
        await this.db.run("update Tokens set expDate = ? where Id = ?", expDate, tokenInfo.Id);
        return tokenInfo.userId;
    }

    async logout(token:string):Promise<boolean>{
        let res = await this.db.run("delete from Tokens where token = ?", token);
        return res.affectedRows>0;
    }
}