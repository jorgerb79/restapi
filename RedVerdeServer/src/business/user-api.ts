import { DBConnectionPool } from '../shared/db-connector';
import { ClientError, ServerError } from '../shared/error-codes';
import { User } from '../shared/dtos';
import { RestError } from '../lib/ServiceBase';
import { isAlphanumeric, isEmail } from 'validator';
import sha1 = require('sha1');
let gmail = require("gmail-send");

export class UserAPI {

    private tokenExpTime = 30*24*3600*1000;
    private validTextRegex = /^[A-Z0-9ÁÉÍÓÚáéíóúÑñÜüäëïöÜü" \;\:\,\.\!\#\·\$\%\&\(\)\=\?\¡\¿\^\*\`\+\´\ç\{\}\[\]\-\_\|\@\<\>]*$/i;

    constructor(
        private db:DBConnectionPool
    ){}

    private validateField(field:string, value:string){
        if( !isAlphanumeric(field) ) {
            let e = ClientError.InvalidParamName;
            throw new RestError(e.httpError, e.code, `Invalid field "${field}"`);
        }
        if( !this.validTextRegex.test(value) ){
            let e = ClientError.InvalidParamValue;
            throw new RestError(e.httpError, e.code, `Invalid value for "${field}"`);
        }
        return value;
    }

    async userExists(email:string):Promise<boolean> {
        try {
            let query = `SELECT Id FROM Users WHERE correo = ?`;
            let res = await this.db.get(query, email);
            return res != null;
        } catch (error) {
            if( error instanceof RestError ){ 
                throw error 
            } else {
                let e = ClientError.InvalidParamValue;
                throw new RestError(e.httpError, e.code, error.message);
            }
        }
    }

    async createUser(user:User):Promise<number>{
        try {
            if( /^\w+\:\:/.test(user.correo) ) {
                user.confirmed = 1;
                user.confirmationCode = null;
            } else { 
                user.confirmationCode = sha1(`${Date.now()}-AppVerde-${user.nombre}-${user.correo}-ConfirmationCode`).toString();
            }
            user.creationDate = (new Date()).toISOString();
            let fields:string[] = [];
            let values:string[] = [];
            let placeHolders = [];
            let info = user as any;
            for(let f in user){
                this.validateField(f, info[f]);
                fields.push(f);
                values.push(info[f]);
                placeHolders.push('?');
            }
            let insertQuery = `Insert into users (${fields.join(",")}) values (${placeHolders.join(",")})`;
            let res = await this.db.run(insertQuery, ...values);
            if( !/^\w+\:\:/.test(user.correo) ) {
                await this.sendConfirmation(user.correo, user.confirmationCode||'');
            }
            return res.insertId;
        } catch(error) {
            if( error instanceof RestError ){ 
                throw error 
            } else {
                if(error instanceof Error){
                    let err = error as Error;
                    if(err.message && err.message.toLowerCase().indexOf('duplicate') ){
                        let e = ServerError.DBDuplicatedValue;
                        throw new RestError(e.httpError, e.code, `Email ${user.correo} is already registered`);
                    }
                }
                let e = ServerError.DBConstraint;
                throw new RestError(e.httpError, e.code, error.message);
            }
        }
    }

    async getUser(userId:number):Promise<User>{
        let sql = `
            select 
                nombre, domicilio, telefono, sexo, tipoUsuario, correo, points, picture,
                fbId, gplusId
            from Users where Id = ?
        `;
        let userInfo = await this.db.get<any>(sql, userId);
        return userInfo;
    }

    async updateUser(userId:number, userInfo:User){
        try {
            let fields:string[] = [];
            let values:string[] = [];
            let placeHolders = [];
            let info = userInfo as any;
            for(let f in userInfo){
                this.validateField(f, info[f]);
                fields.push(`${f} = ?`);
                values.push(info[f]);
                placeHolders.push('?');
            }
            values.push(userId.toString());
            let updateQuery = `update users set ${fields.join(",")} where Id = ?`;
            await this.db.run(updateQuery, ...values);
        } catch(error) {
            if( error instanceof RestError ){ 
                throw error 
            } else {
                let e = ServerError.DBConstraint;
                throw new RestError(e.httpError, e.code, error.message);
            }
        }
    }

    async updateField(userId:number, field:string, value:string|number){
        this.validateField(field, value.toString());
        await this.db.run(`update Users set ${field} = ? where Id = ?`, value, userId);
    }

    async sendConfirmation(emailAddress:string, confirmationCode:string):Promise<boolean>{
        return new Promise<boolean>((resolve,reject)=>{
            if( !isEmail(emailAddress) ){
                let e = ClientError.InvalidParamValue;
                throw new RestError(e.httpError, e.code, "Invalid email address");
            }
            resolve(true);
            /* 
            let mail = gmail({
                user: "nmcreaturetester1@gmail.com",
                pass: "NWcr@7u43",
                to: emailAddress,
                subject: "Confirmación de correo Red Verde",
                html: `
                |<h2>Bienvenido a Red Verde</h2>
                |<p>
                | Para completar su registro es necesario confirmar su correo electrónico.
                |</p>
                |
                |Haga click en la siguiente liga para confirmar su cuenta de correo: <a href="http://localhost:8080/confirm/${confirmationCode}">CONFIRMAR</a>
                |
                `.replace(/^\s*\|/gm,"")
            });
            mail({}, (err:Error, res:any)=>{
                if(err){
                    reject(err)
                } else {
                    resolve(res);
                }
            });
            */
        });
    }

    async confirmMail(confirmationCode:string):Promise<boolean>{
        let expTimeMS = Date.now() - this.tokenExpTime;
        let expTime = (new Date(expTimeMS)).toISOString();
        await this.db.run("Delete from Users where confirmed = 0 and creationDate <= ?", expTime);
        let r = await this.db.run("Update Users set confirmed = 1 where confirmationCode = ?", confirmationCode);
        if(r.affectedRows==0){
            let e = ClientError.InvalidToken;
            throw new RestError(e.httpError, e.code, "Invalid or expired Token");
        }
        return true;
    }
}