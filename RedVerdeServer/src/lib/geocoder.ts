import request = require("request");

export interface GeoSearchResult {
    results: Result[];
    status: string;
}

export interface Result {
    'address_components': Addresscomponent[];
    'formatted_address': string;
    geometry: Geometry;
    'partial_match': boolean;
    'place_id': string;
    types: string[];
}

export interface Geometry {
    bounds: Bounds;
    location: Northeast;
    'location_type': string;
    viewport: Bounds;
}

export interface Bounds {
    northeast: Northeast;
    southwest: Northeast;
}

export interface Northeast {
    lat: number;
    lng: number;
}

export interface Addresscomponent {
    'long_name': string;
    'short_name': string;
    types: string[];
}

export function asyncGeocode(searchKey:string):Promise<Result[]>{
    let uri = `https://maps.googleapis.com/maps/api/geocode/json?address=${encodeURIComponent(searchKey)}`;
    return new Promise<Result[]>((resolve,reject)=>{
        request(uri, (error, resp)=>{
            if(error){
                reject(error);
            } else if( resp.statusCode != 200 ) {
                reject(resp.body);
            } else {
                let response = (<GeoSearchResult>JSON.parse(resp.body));
                if(response.status == "ZERO_RESULTS"){
                    resolve(<Result[]>[]);
                } else if( response.status == "OK") {
                    resolve(response.results);
                } else {
                    reject(response.status);
                }
            }
        });
    });
}