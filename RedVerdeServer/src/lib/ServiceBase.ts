
import restify = require('restify');

export enum HTTPMethod {
    GET,
    POST,
    DEL,
    PUT
}

export enum DataType {
    JSON = <any>'application/json',
    PLAIN = <any>'text/plain',
    HTML = <any>'text/html',
    RAW = <any>'application/octet-stream'
}

class BindData{
    constructor(
        public method:HTTPMethod,
        public path:string | RegExp,
        public handler:Function,
        public respType:DataType
    ){}
}

export interface ServerOptions extends restify.ServerOptions {
    debug?:boolean
}

export interface Request extends restify.Request {
}

export interface Response extends restify.Response {
}

export class RestError extends Error {
    public httpCode:number
    public code:number;

    constructor(httpCode:number, code:number, message?:any){
        super(message);
        this.httpCode = httpCode;
        this.code = code;
    }
}

export function GET(path:string | RegExp, respType=DataType.JSON){
    return function (target:any, propertyKey: string, descriptor: PropertyDescriptor) {
        let handler = target[propertyKey] as Function
        if( !(target instanceof ServiceBase) ){
            throw new Error("Method decorator can only be applied into ServiceBase subclasess.")
        }
        if(target.bindData === undefined){
            target.bindData = [];
        }
        target.bindData.push( new BindData(HTTPMethod.GET, path, handler, respType) );
    }
}

export function POST(path:string | RegExp, respType=DataType.JSON){
    return function (target:any, propertyKey: string, descriptor: PropertyDescriptor) {
        let handler = target[propertyKey] as Function
        if( !(target instanceof ServiceBase) ){
            throw new Error("Method decorator can only be applied into ServiceBase subclasess.")
        }
        if(target.bindData === undefined){
            target.bindData = [];
        }
        target.bindData.push( new BindData(HTTPMethod.POST, path, handler, respType) );
    }
}

export function PUT(path:string | RegExp, respType=DataType.JSON){
    return function (target:any, propertyKey: string, descriptor: PropertyDescriptor) {
        let handler = target[propertyKey] as Function
        if( !(target instanceof ServiceBase) ){
            throw new Error("Method decorator can only be applied into ServiceBase subclasess.")
        }
        if(target.bindData === undefined){
            target.bindData = [];
        }
        target.bindData.push( new BindData(HTTPMethod.PUT, path, handler, respType) );
    }
}

export function DEL(path:string | RegExp, respType=DataType.JSON){
    return function (target:any, propertyKey: string, descriptor: PropertyDescriptor) {
        let handler = target[propertyKey] as Function
        if( !(target instanceof ServiceBase) ){
            throw new Error("Method decorator can only be applied into ServiceBase subclasess.")
        }
        if(target.bindData === undefined){
            target.bindData = [];
        }
        target.bindData.push( new BindData(HTTPMethod.DEL, path, handler, respType) );
    }
}

export class ServiceBase {
    public bindData:BindData[];
    private server:restify.Server;

    constructor(options?:ServerOptions){
        this.server = restify.createServer(options);
        this.server.on('NotFoundError', function (req:Request, res:Response, err:any, cb:Function) {
            err.body = 'something is wrong!';
            return cb();
        });
        this.server.use(restify.plugins.queryParser());
        this.server.use(restify.plugins.bodyParser());
        let debug = options && options.debug;
        for(let b of this.bindData){
            this.bind(b.method, b.path, b.handler, b.respType, debug);
        }
    }

    bind(method:HTTPMethod, path:string|RegExp, callback:Function, respType:DataType, debug:boolean=false){
        if( typeof(path) === 'string' && path.substr(0,3) === 'RE:' ){
            let pathParts = path.substr(3).split("|");
            let pattern = pathParts[0];
            let flags = pathParts.length>1 ? pathParts[1] : "";
            path = new RegExp(pattern, flags);
        }

        let handler = async (req:Request, resp:Response)=>{
            try {
                if(debug){
                    console.log(` - Called: ${HTTPMethod[method]} "${req.getPath()}" from: "${req.connection.remoteAddress}:${req.connection.remotePort}"`);
                }
                let response = await callback.apply(this, [req, resp]);
                resp.setHeader('content-type', respType.toString());
                if(response instanceof Buffer){
                    resp.writeHead(200, {
                        'Content-Lenght': response.byteLength,
                        'Content-Type': respType.toString()
                    });
                    resp.write(response);
                    resp.end();
                } else {
                    resp.send(200, response);
                }
            } catch(error){
                if( error instanceof RestError){
                    resp.send(error.httpCode, `{ "code":"${error.code}", "message":"${error.message}" }`);
                } else {
                    resp.send(500, `{ "code": -1, "message":"${error.message}" }`);
                }
            }
        }
        switch(method){
            case HTTPMethod.GET:
                if(debug){
                    console.log(`Binding: GET ${path}`);
                }
                this.server.get(path, handler);
                break;
            case HTTPMethod.POST:
                if(debug){
                    console.log(`Binding: POST ${path}`);
                }
                this.server.post(path, handler);
                break;
            case HTTPMethod.PUT:
                if(debug){
                    console.log(`Binding: PUT ${path}`);
                }
                this.server.put(path, handler);
                break;
            case HTTPMethod.DEL:
                if(debug){
                    console.log(`Binding: DELETE ${path}`);
                }
                this.server.del(path, handler);
        }
    }

    listen(port=8080){
        this.server.listen(port);
        console.log(`Server listening on: http://localhost:${port}/`);
    }

}