
import fs = require('fs');

export function readFile(fpath:string):Promise<Buffer>{
    return new Promise<Buffer>((resolve,reject)=>{
        fs.readFile(fpath, (err, data)=>{
            if(err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    });
}

export function getTypesFilter(types:number, typeField='tipoPropiedadId'){
        let typesFilter:string[] = [];
        if( (types&1)>0 ) typesFilter.push(`${typeField} = 1`);
        if( (types&2)>0 ) typesFilter.push(`${typeField} = 2`);
        if( (types&4)>0 ) typesFilter.push(`${typeField} = 3`);
        if( (types&8)>0 ) typesFilter.push(`${typeField} = 4`);
        let typesCond = typesFilter.length>0 ? `(${typesFilter.join(' OR ')})` : `${typeField} = 0`;
        return typesCond;
    }

export function getSearchKeyFilter(searchKey:string, returnEmpty=false):string {
    searchKey = searchKey.trim();
    if(returnEmpty && searchKey.length==0) 
        return '';
    if(searchKey.length>0) {
        searchKey = '%' + searchKey.toLocaleLowerCase().replace(/\s+/g, `%`) + '%';
    } else {
        searchKey = '%';
    }
    return searchKey;
}