import { NetworkingAPI } from './business/network-api';
import { Element, ElementImage, LoginInfo, ExistsResp } from './shared/dtos';
import { ClientError, ServerError } from './shared/error-codes';
import { readFile } from './lib/utils';
import { DBConnectionPool, PrepareDB } from './shared/db-connector';
import { AuthAPI } from './business/auth-api';
import { UserAPI } from './business/user-api';
import { DataType, DEL, GET, POST, PUT, Request, Response, RestError, ServerOptions, ServiceBase } from './lib/ServiceBase';
import { ElementAPI } from "./business/element-api";

class GenericResponse<T>{
    constructor(
        public code:number,
        public response:T
    ){}
}

class RedVerdeServer extends ServiceBase {
    
    private authAPI:AuthAPI;
    private userAPI:UserAPI;
    private elementAPI:ElementAPI;
    private netAPI:NetworkingAPI;

    constructor(db:DBConnectionPool, options?:ServerOptions){
        super(options);
        this.authAPI = new AuthAPI(db);
        this.userAPI = new UserAPI(db);
        this.elementAPI = new ElementAPI(db);
        this.netAPI = new NetworkingAPI(db);
    }

    private async validateUser(req:Request, required:boolean=true):Promise<number> {
        try {
            let h = req.headers;
            let token = h["auth-token"] as string;
            if(!token){
                if(required){
                    throw new Error();
                } else {
                    return 0;
                }
            }
            let userId = await this.authAPI.getUserId(token);
            return userId
        } catch( error ){
            let e = ClientError.Unauthorized;
            throw new RestError(e.httpError, e.code, "Unauthorized client (invalid token)");
        }
    }

    @GET("/userExists/:email")
    async userExists(req:Request, resp:Response) {
        let email = req.params.email;
        let r = await this.userAPI.userExists(email);
        return <ExistsResp>{exists: r};
    }

    @POST("/login")
    async login(req:Request, resp:Response){
        let b = req.body as LoginInfo;
        let r = await this.authAPI.login(b.correo, b.password);
        return r;
    }

    @DEL("/login")
    async logout(req:Request, resp:Response){
        let h = req.headers;
        let token = h["auth-token"] as string;
        let success = await this.authAPI.logout(token);
        return new GenericResponse(0, success);
    }

    @POST("/register")
    async createUser(req:Request, resp:Response){
        await this.userAPI.createUser(req.body);
        return new GenericResponse(0, "OK");
    }

    @GET("/profile")
    async getProfile(req:Request, resp:Response){
        let userId = await this.validateUser(req);
        let r = await this.userAPI.getUser(userId);
        return r;
    }

    @POST("/updateProfile")
    async updateUser(req:Request, resp:Response){
        let userId = await this.validateUser(req);
        await this.userAPI.updateUser(userId, req.body);
        return new GenericResponse(0, "OK");
    }

    @GET("/confirm/:code", DataType.HTML)
    async confirmEmail(req:Request, resp:Response){
        let confirmed = await this.userAPI.confirmMail(req.params.code);
        if(confirmed){
            let html = await readFile("confirm.html");
            return html;    
        } else {
            let html = await readFile("confirm-invalid.html");
            return html;    
        }
        
    }

    @POST("/element")
    async createElement(req:Request, resp:Response){
        let userId = await this.validateUser(req);
        let element = req.body;
        if(element['Id']) delete element['Id'];
        element.administradorId = userId;
        let Id = await this.elementAPI.createElement(element);
        if(!Id){
            let e = ServerError.DBErrorQuery;
            throw new RestError(e.httpError, e.code, "Unable to create element");
        }
        return new GenericResponse<number>(0, Id);
    }

    @GET("/element/:elementId")
    async getElement(req:Request, resp:Response){
        let elementId = req.params.elementId;
        let r = await this.elementAPI.getElement(elementId);
        return r;
    }

    @PUT("/element/:elementId")
    async updateElement(req:Request, resp:Response){
        let userId = await this.validateUser(req);
        let elementId = req.params.elementId;
        let element = req.body;
        element.administradorId = userId;
        await this.elementAPI.updateElement(elementId, element);
        return new GenericResponse(0, "OK");
    }

    @DEL("/element/:elementId")
    async deleteElement(req:Request, resp:Response){
        let userId = await this.validateUser(req);
        let elementId = req.params.elementId;
        let element = await this.elementAPI.getElement(elementId);
        if(element.administradorId != userId){
            let e = ClientError.Unauthorized;
            throw new RestError(e.httpError, e.code, "You are not the administrator of this Element");
        }
        await this.elementAPI.deleteElement(elementId);
        return new GenericResponse(0, "OK");
    }

    @GET("/images/:elementId")
    async getImages(req:Request, resp:Response){
        let elementId = req.params.elementId as number;
        let images = await this.elementAPI.getImages(elementId);
        return {
            images: images
        };
    }

    @POST("/image")
    async registerImage(req:Request, resp:Response){
        let userId = await this.validateUser(req);
        let image = req.body as ElementImage;;
        let Id = await this.elementAPI.registerImage(image.elemementId, image.imagePath);
        if(!Id){
            let e = ServerError.DBErrorQuery;
            throw new RestError(e.httpError, e.code, "Unable to register Image");
        }
        return new GenericResponse<number>(0, Id);
    }

    @DEL("/image/:imageId")
    async deleteImage(req:Request, resp:Response){
        let userId = await this.validateUser(req);
        let imageId = req.params.imageId as number;
        await this.elementAPI.removeImage(imageId);
        return new GenericResponse(0, "OK");
    }

    @GET("/getMarkers") // ?startLat=N&startLong=N&endLat=N&endLong=N&types=N
    async getMarkers(req:Request, resp:Response){
        let p = req.query;
        let startLat = p.startLat;
        let startLong = p.startLong;
        let endLat = p.endLat;
        let endLong = p.endLong;
        let types = p.types;
        if(types==null || !(types as string).match(/^\d+$/)){
            let e = ClientError.InvalidParamValue;
            throw new RestError(e.httpError, e.code, 'Invalid value for param "types"');
        }
        let markers = await this.elementAPI.getMarkers(startLat, startLong, endLat, endLong, types);
        return {
            markers: markers
        };
    }

    @GET("/getMarkDetail/:elementId")
    async getMarkDetail(req:Request, resp:Response){
        let userId = await this.validateUser(req, false);
        let elementId = req.params.elementId;
        let result = await this.elementAPI.getMarkDetail(elementId, userId);
        return result;
    }

    @GET("/search/:searchKey")
    async search(req:Request, resp:Response){
        let searchKey = req.params.searchKey || '';
        let res = await this.elementAPI.search(searchKey);
        return {
            elements: res
        };
    }

    @POST("/network/:elementId")
    async addToMyNetwork(req:Request, resp:Response){
        let userId = await this.validateUser(req);
        let elementId = req.params.elementId || '';
        if( !elementId.toString().match(/^\d+$/) ){
            let e = ClientError.InvalidParamValue;
            throw new RestError(e.httpError, e.code, 'Invalid value for elementId');
        }
        await this.netAPI.addToMyNetwork(userId, elementId);
        return new GenericResponse<boolean>(0, true);
    }

    @DEL("/network/:elementId")
    async removeFromNetwork(req:Request, resp:Response){
        let userId = await this.validateUser(req);
        let elementId = req.params.elementId || '';
        if( !elementId.toString().match(/^\d+$/) ){
            let e = ClientError.InvalidParamValue;
            throw new RestError(e.httpError, e.code, 'Invalid value for elementId');
        }
        await this.netAPI.removeFromNetwork(userId, elementId);
        return new GenericResponse<boolean>(0, true);
    }

    @GET("/network/admin") // ?searchKey=S&types=N
    async myNetworkAdmin(req:Request, resp:Response){
        let userId = await this.validateUser(req);
        let types = req.query['types'] || (1|2|4|8).toString();
        if(!types.toString().match(/^\d+$/)){
            let e = ClientError.InvalidParamValue;
            throw new RestError(e.httpError, e.code, 'Invalid value for param "types"');
        }
        let searchKey = req.query['searchKey'] || '';
        let network = await this.netAPI.getMyNetworkAdmin(userId, types, searchKey);
        return {
            elements: network
        }
    }

    @GET("/network/participant") // ?searchKey=S&types=N
    async myNetworkParticipant(req:Request, resp:Response){
        let userId = await this.validateUser(req);
        let types = req.query['types'] || (1|2|4|8).toString();
        if(!types.toString().match(/^\d+$/)){
            let e = ClientError.InvalidParamValue;
            throw new RestError(e.httpError, e.code, 'Invalid value for param "types"');
        }
        let searchKey = req.query['searchKey'] || '';
        let network = await this.netAPI.getMyNetworkParticipant(userId, types, searchKey);
        return {
            elements: network
        }
    }
}

PrepareDB(true).then(db=>{
    let srv = new RedVerdeServer(db, { debug: true });
    srv.listen(8080);
});

