import { getSearchKeyFilter, getTypesFilter } from './lib/utils';
import sha1 = require('sha1');

import { AuthAPI } from './business/auth-api';
import { ElementAPI } from './business/element-api';
import { UserAPI } from './business/user-api';
import { asyncGeocode } from './lib/geocoder';
import { PrepareDB } from './shared/db-connector';
import { Element, User, SearchResult } from './shared/dtos';
import assert = require('assert');
import { NetworkingAPI } from "./business/network-api";

function delay(sec:number):Promise<void>{
    return new Promise<void>(resolve=>{
        setTimeout(()=>{
            resolve();
        }, sec*1000);
    });
}

async function testAuth() {
    let db = await PrepareDB();
    let authApi = new AuthAPI(db);
    let r1 = await authApi.login("usuario1@server.com","12341234");

    let userId = await authApi.getUserId(r1.token);
    console.log(`UserId: ${userId}`)

    let tokens = await db.all<any>("select * from Tokens");
    console.log( JSON.stringify(tokens) );

    await delay(10);

    userId = await authApi.getUserId(r1.token);
    console.log(`UserId: ${userId}`)
    
    tokens = await db.all<any>("select * from Tokens");
    console.log( JSON.stringify(tokens) );

    let success = await authApi.logout(r1.token);
    console.log(`LogoutSuccess: ${success}`);
}

async function testUser(){
    let db = await PrepareDB();
    let authApi = new AuthAPI(db);
    let usrApi = new UserAPI(db);
    if( usrApi.userExists("redverdetest@server.com") ){
        return;
    }
    let user:User = {
        nombre: "Red Verde",
        correo: "redverdetest@server.com",
        password: "12345",
        sexo: "M",
        telefono: "33 66 77 55 44",
        tipoUsuario: "C"
    };
    let newUserId = await usrApi.createUser(user);

    await usrApi.updateField(newUserId, "confirmationCode", sha1(Date.now().toString()).toString());
    await usrApi.updateField(newUserId, "confirmed", 1);

    let users = await db.all<any>("select * from Users");
    let tokenResp = await authApi.login("usuario1@server.com","12341234");
    let userId = await authApi.getUserId(tokenResp.token);
    user.nombre = "A Person Name";
    await usrApi.updateUser(userId, user);
    let updatedUser = await usrApi.getUser(userId);
    updatedUser = await usrApi.getUser(userId);
}

async function testElement(){
    let db = await PrepareDB();
    let authApi = new AuthAPI(db);
    let elementApi = new ElementAPI(db)

    let authResp = await authApi.login("usuario1@server.com","12341234");
    let userId = await authApi.getUserId(authResp.token);

    let element:Element = {
        nombre: "Lote 1",
        tipoPropiedadId: 1,
        lat: 20.6762102,
        lng: -103.4251827
    }

    element.administradorId = userId;
    let newElementId = await elementApi.createElement(element);
    let newElement = await elementApi.getElement(newElementId);
    newElement.nombre = "Lote de Don Yorch";
    await elementApi.updateElement(newElement.Id||0, newElement);
    await elementApi.updateElementField(newElementId, "domicilio", "Miguel Angel 96 Int 6, Real Vallarta, Zapopan, Jalisco, MX.");
    await elementApi.deleteElement(newElementId);
    
    let elements:Element[] = [
        {nombre: "Lote 1", tipoPropiedadId: 1, lat: 20.675363, lng: -103.425928, administradorId: userId, points: 3},
        {nombre: "Lote 2", tipoPropiedadId: 1, lat: 20.676779, lng: -103.419748, administradorId: userId, points: 2},
        {nombre: "Lote 3", tipoPropiedadId: 2, lat: 20.675363, lng: -103.425928, administradorId: userId, points: 2.5},
        {nombre: "Lote 4", tipoPropiedadId: 4, lat: 20.670334, lng: -103.427462, administradorId: userId, points: 1.5}
    ];
    for(let element of elements){
        await elementApi.createElement(element);
    }
    let marks = await elementApi.getMarkers(20.675363, -103.427462, 20.676779, -103.419748, 15);
    console.log(marks.length);
}

async function testMarks(){
    let db = await PrepareDB();
    let elementApi = new ElementAPI(db);
    let res = await elementApi.getMarkers(20.5, -103.5, 20.7, -103.3, 15);
    console.log(JSON.stringify(res,null,4));
}

async function testGeocoder() {
    let address = "miguel angel 96, zapopan"
    let results = await asyncGeocode(address);
    console.log(JSON.stringify(results.map(r=><any>{
        address: r.address_components.map(c=><any>{value: c.long_name, type: c.types[0]}),
        location:r.geometry.location,
        loc: `${r.geometry.location.lng}, ${r.geometry.location.lng}`
    }),null,4));
}

async function testSearch() {
    let searchKey = "tesistan"
    let db = await PrepareDB();
    let elementApi = new ElementAPI(db);
    let res = await elementApi.search(searchKey);
    console.log(JSON.stringify(res,null,4));
}

async function testMarkDetail() {
    let searchKey = "tesistan"
    let db = await PrepareDB();
    let elementApi = new ElementAPI(db);
    let marks = await elementApi.search("tesistan");
    assert( marks.length>0, "No marks to work with");
    let detail = await elementApi.getMarkDetail(marks[0].elementId, 0);
    console.log(JSON.stringify(detail,null,4));
}

async function testNetworking() {
    let db = await PrepareDB(false);
    let authApi = new AuthAPI(db);
    let netApi = new NetworkingAPI(db);
    let login = await authApi.login("usuario1@server.com","12341234");
    let userId = await authApi.getUserId(login.token);
    let result1 = await netApi.getMyNetworkParticipant(userId, 1|2|4|8, 'Zapotlanejo');
    let result2 = await netApi.getMyNetworkAdmin(userId, 1|2|4|8, 'Zapotlanejo');
    result1.length>0 && result2.length>0;
}

async function testSQL(){
    let dbPool = await PrepareDB();
    
    try {
        let userId = 3;
        let searchKey = 'usuario 1';
        let types = 1|2|4|8;
        let typesFilter = getTypesFilter(types, 'e.tipoPropiedadId');
        let query = `
            select
                e.Id as elementId, 
                e.nombre,
                e.domicilio,
                u.nombre as administrador,
                n.userId,
                e.administradorId,
                numberPoints as points
            from Elements e
            join Users u on u.Id = e.administradorId
            join Network n on n.elementId = e.Id
            where (n.userId = ? and e.administradorId <> ?) and concat_ws(' ', coalesce(nombre,''), coalesce(domicilio,''), coalesce(u.nombre,'')) like ? and ${typesFilter}
            order by e.nombre, e.domicilio
        `;
        searchKey = getSearchKeyFilter(searchKey);
        let rows = await dbPool.all(query, userId, userId, searchKey);
        console.log(JSON.stringify(rows,null,4));
    } catch(error) {
        console.log((error as Error).message);
    }
    
    process.exit(0);
}

async function test(){
    try {
        await testAuth();
        await testUser();
        await testElement();
        await testMarks();
        await testGeocoder();
        await testSearch();
        await testMarkDetail();
        await testNetworking();
        //await testSQL();
    } catch(error){
        console.log(error.message);
        console.log(error);
    }
    process.exit(0);
}
test();