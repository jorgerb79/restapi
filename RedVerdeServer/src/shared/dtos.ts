export interface LoginResp {
    token: string;
    name: string;
    urlImagesBase: string;
}

export interface LoginInfo {
    correo:string;
    password:string;
}

export interface Element {
    Id?: number;
    nombre: string;
    descripcion?: string;
    tipoPropiedadId: number;
    lat: number;
    lng: number;
    fotoFolder?: string;
    telefono?: string;
    correo?: string;
    horario?: string;
    domicilio?: string;
    precioMin?: string;
    precioMax?: string;
    administradorId?: Number;
    superficie?: string;
    textoAgendaTrabajo?: string;
    points?: number;
    creationDate?: string;
    isAdded?:number;
}

export interface ElementImage {
    Id?: number;
    elemementId: number;
    imagePath: string;
    points: number;
}

export interface Marker {
    elementId: number;
    lat: number;
    lng: number;
    tipoPropiedadId: number;
}

export interface MarkerDetail {
    elementId: number;
    nombre: string;
    domicilio: string;
    administrador?: string;
    lat: number;
    lng: number;
    points?: number;
    isAdmin: boolean;
    isAdded: boolean;
    imagen?:string;
}

export interface User {
    Id?:number;
    nombre: string;
    domicilio?: string;
    password: string;
    telefono?: string;
    sexo?: string;
    tipoUsuario: string;
    correo: string;
    points?: number;
    fbId?: string;
    gpplusId?: string;
    confirmationCode?: string|null;
    confirmed?: number;
    picture?: string;
    creationDate?: string;
    active?: number;
}

export interface GeoPos {
    lat: number;
    lng: number;
}

export interface SearchResult {
  elementId: number;
  tipoPropiedadId: number;
  lat: number;
  lng: number;
  nombre: string;
  fotoFolder: string;
  domicilio: string;
  points: number;
}

export interface NetworkResp {
    elements:MarkerDetail[]
}

export interface ExistsResp {
    exists:boolean;
}