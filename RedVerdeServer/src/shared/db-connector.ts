import { PoolConnection, createPool, PoolConfig, Pool, FieldInfo } from 'mysql';
import { Element} from '../shared/dtos';
import * as fs from "fs";

interface IDBConnection {
    get<T>(query:string, ...values:any[]):Promise<T|null>;
    run<T>(query:string, ...values:any[]):Promise<QueryResult<T>>;
    all<T>(query:string, ...values:any[]):Promise<QueryResult<T>>;
}

export interface QueryResult<T> extends Array<T> {
    insertId:number;
    affectedRows:number;
    changedRows:number;
    fields:FieldInfo;
}

export class DBConnectionPool implements IDBConnection {

    private mysqlPool:Pool;

    constructor(
        private settings:PoolConfig
    ){
        this.mysqlPool = createPool(settings);
    }

    getConnection():Promise<DBConnection> {
        return new Promise((resolve, reject)=>{
            this.mysqlPool.getConnection((err, con)=>{
                if(err!=null) reject(err);
                resolve(new DBConnection(con));
            });
        });
    }

    public async get<T=any>(query: string, ...values: any[]): Promise<T|null> {
        let con = await this.getConnection();
        try {
            let res = await con.get(query, ...values);
            return res;
        } finally {
            con.release();
        }
    }

    public async run<T=any>(query: string, ...values: any[]): Promise<QueryResult<T>> {
        let con = await this.getConnection();
        try {
            return await con.run(query, ...values);
        } finally {
            con.release();
        }
    }

    public async all<T=any>(query: string, ...values: any[]): Promise<QueryResult<T>> {
        let con = await this.getConnection();
        try {
            return await con.all(query, ...values);
        } finally {
            con.release();
        }
    }

    async exec<T=any>(block:(connection:DBConnection)=>Promise<T>):Promise<T> {
        let con = await this.getConnection();
        try {
            return await block(con);
        } finally {
            con.release();
        }
    }
}


export class DBConnection implements IDBConnection {

    constructor(
        private con:PoolConnection
    ){}

    public async query<T>(query: string, values: any[]): Promise<QueryResult<T>> {
        return new Promise<QueryResult<T>>((resolve, reject)=>{
            this.con.query(query, values, (error, result, fields)=>{
                if(error) {
                    reject(error);
                } else {
                    result.fields = fields;
                    resolve(result);
                }
            });
        });
    }

    public async get<T=any>(query: string, ...values: any[]): Promise<T|null> {
        let res = await this.query<T>(query, values);
        return res.length>0 ? res[0] : null;
    }

    public async run<T=any>(query: string, ...values: any[]): Promise<QueryResult<T>> {
        return await this.query<T>(query, values);
    }

    public async all<T=any>(query: string, ...values: any[]): Promise<QueryResult<T>> {
        return await this.query<T>(query, values);
    }

    public release() {
        this.con.release();
    }
}


export async function PrepareDB(resetDB:boolean=false, settingsPath:string='../db-config.cfg'):Promise<DBConnectionPool> {
    settingsPath = settingsPath.trim();
    let settingsFile = fs.readFileSync(settingsPath).toString();
    let settings = JSON.parse(settingsFile);
    let pool = new DBConnectionPool(settings);
    if(resetDB){
        let dbSchema = fs.readFileSync("../db-schema.sql").toString();
        let queries = dbSchema.split(/;/g);
        await pool.exec(async con=>{
            for(let q of queries){
                q = q.trim();
                if(q.length==0) continue;
                try {
                    await pool.run(q);
                } catch(error){
                    console.log(`===> FAILED TO EXECUTE: ${q.substr(0,30).replace('\n',' ')}\n===> WITH ERROR: ${error.message}\n`);
                    break;
                }
            }
            let elements = await con.all<Element>('select * from Elements');
            for(let element of elements){
                let elementId = element.Id||0;
                let resp = await con.get(`
                    select elementId, sum(numberPoints)/count(numberPoints) as average from Network
                    where elementId = ? group by elementId order by elementId`, elementId);
                if(resp){
                    await con.run('update Elements set points = ? where Id = ?', Math.ceil(resp.average), elementId);
                }
            }
        });
    }
    return Promise.resolve(pool);
}