
export interface ErrorValue {
    httpError: number;
    code: number;
}

export class ServerError {
    static InternalServerError:ErrorValue = {httpError: 500, code: 1} 
    static DBErrorQuery:ErrorValue = {httpError: 500, code: 2} 
    static DBNoRecord:ErrorValue = {httpError: 500, code: 3} 
    static DBConstraint:ErrorValue = {httpError: 500, code: 4} 
    static NotImplemented:ErrorValue = {httpError: 501, code: 5}
    static DBDuplicatedValue:ErrorValue = {httpError: 500, code: 6} 
}

export class ClientError {
    static BadRequest:ErrorValue = {httpError: 400, code: 1} 
    static InvalidParamName:ErrorValue = {httpError: 400, code: 2}
    static InvalidParamValue:ErrorValue = {httpError: 400, code: 3}
    static MissingParam:ErrorValue = {httpError: 400, code: 4}
    static InvalidToken:ErrorValue = {httpError: 401, code: 5}
    static InvalidCredentials:ErrorValue = {httpError: 401, code: 6}
    static Unauthorized:ErrorValue = {httpError: 403, code: 7}
    static InvalidUserPassword:ErrorValue = {httpError: 401, code: 8}
    static UnConfirmedEmail:ErrorValue = {httpError: 401, code: 9}
}
