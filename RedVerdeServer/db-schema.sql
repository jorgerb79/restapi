

DROP TABLE IF EXISTS Users;
CREATE TABLE Users (
    Id int not null primary key AUTO_INCREMENT,
    nombre varchar(100) not null,
    domicilio varchar(250),
    password varchar(40),
    telefono varchar(30),
    sexo char(1),
    tipoUsuario varchar(100) not null,
    correo varchar(30),
    points decimal(2,1) default 1,
    fbId varchar(100),
    gplusId varchar(100),
    confirmationCode varchar(100),
    confirmed tinyint default 0,
    picture varchar(250),
    creationDate datetime not null,
    active tinyint default 1,
    CONSTRAINT UC_CORREO UNIQUE (correo)
);

INSERT INTO Users (Id, nombre, correo, password, domicilio, telefono, sexo, tipoUsuario, confirmed, creationDate) values
    (1, 'Administrador', 'admin@redverde.com.mx', '2235235234232', 'Domicilio 1', '', 'M', 'C', 1, '2017-05-31T01:35:09.853Z')
;

DROP TABLE IF EXISTS Tokens;
CREATE TABLE Tokens (
    Id int not null primary key AUTO_INCREMENT,
    userId int,
    token varchar(100),
    expDate datetime
);

DROP TABLE IF EXISTS Network;
CREATE TABLE Network (
    userId int not null,
    elementId int not null,
    numberPoints int not null default 0,
    constraint NetworkPK primary key (userId, elementId)
);
INSERT INTO Network (userId, elementId, numberPoints) values
    (1, 55, 1), (1, 59, 1), (1, 80, 3), (1, 124, 1), (1, 143, 3), (1, 201, 3), (1, 215, 3),
    (1, 216, 1), (1, 231, 2), (1, 248, 2), (1, 255, 1), (1, 316, 0), (1, 352, 3), (1, 356, 3),
    (1, 358, 1), (1, 372, 0), (1, 387, 0), (1, 511, 0), (1, 623, 0), (1, 624, 3), (1, 689, 2),
    (1, 712, 0), (1, 785, 2), (1, 798, 2), (1, 807, 3), (1, 841, 1), (1, 846, 1), (1, 847, 1),
    (1, 867, 3), (1, 882, 0), (1, 893, 2), (1, 922, 0), (1, 924, 1), (1, 929, 1), (1, 938, 3),
    (1, 940, 1), (1, 1000, 3), (1, 1033, 0), (1, 1043, 3), (1, 1044, 1), (1, 1086, 3), (2, 65, 2),
    (2, 99, 0), (2, 108, 1), (2, 120, 0), (2, 143, 2), (2, 256, 2), (2, 284, 0), (2, 289, 2),
    (2, 314, 3), (2, 324, 0), (2, 341, 0), (2, 361, 2), (2, 377, 3), (2, 569, 0), (2, 574, 2),
    (2, 592, 2), (2, 599, 2), (2, 633, 3), (2, 638, 0), (2, 713, 2), (2, 724, 0), (2, 835, 2),
    (2, 884, 0), (2, 885, 2), (2, 934, 1), (2, 940, 3), (2, 941, 2), (2, 966, 3), (2, 1037, 1),
    (2, 1045, 1), (2, 1084, 0), (3, 24, 2), (3, 146, 1), (3, 149, 3), (3, 166, 3), (3, 223, 0),
    (3, 280, 3), (3, 371, 1), (3, 412, 0), (3, 442, 2), (3, 463, 0), (3, 472, 0), (3, 557, 3),
    (3, 584, 1), (3, 613, 3), (3, 616, 1), (3, 631, 2), (3, 708, 0), (3, 711, 2), (3, 721, 0),
    (3, 824, 2), (3, 860, 0), (3, 912, 0), (3, 959, 3), (3, 960, 2), (3, 976, 0), (3, 1005, 3),
    (3, 1048, 1), (4, 1, 2), (4, 113, 1), (4, 138, 3), (4, 184, 0), (4, 207, 0), (4, 213, 2),
    (4, 263, 3), (4, 320, 2), (4, 417, 1), (4, 465, 0), (4, 491, 2), (4, 498, 0), (4, 611, 3),
    (4, 639, 1), (4, 640, 0), (4, 643, 3), (4, 681, 1), (4, 700, 0), (4, 730, 1), (4, 734, 1),
    (4, 749, 1), (4, 752, 2), (4, 795, 3), (4, 836, 2), (4, 870, 3), (4, 880, 3), (4, 888, 1),
    (4, 950, 0), (4, 965, 2), (4, 1056, 0), (4, 1070, 3), (4, 1075, 2), (4, 1089, 2), (4, 1097, 1),
    (4, 1116, 2), (5, 15, 0), (5, 91, 3), (5, 109, 3), (5, 178, 3), (5, 179, 0), (5, 242, 3),
    (5, 298, 1), (5, 435, 2), (5, 444, 0), (5, 463, 2), (5, 476, 0), (5, 538, 1), (5, 598, 1),
    (5, 638, 2), (5, 673, 3), (5, 679, 1), (5, 730, 3), (5, 736, 0), (5, 790, 3), (5, 938, 2),
    (5, 1051, 0);

DROP TABLE IF EXISTS ElementTypes;
CREATE TABLE ElementTypes (
    Id int not null primary key,
    type varchar(20) not null
);

insert into ElementTypes values 
    (1, 'Cliente'),
    (2, 'Huerto'),
    (3, 'Proveedor'),
    (4, 'Residuo')
;

DROP TABLE IF EXISTS Elements;
CREATE TABLE Elements (
    Id int not null primary key AUTO_INCREMENT,
    nombre varchar(100) not null,
    descripcion varchar(200) not null,
    tipoPropiedadId tinyint,
    lat decimal(9,6) not null,
    lng decimal(9,6) not null,
    fotoFolder varchar(512),
    telefono varchar(30),
    correo varchar(30),
    horario varchar(30),
    domicilio varchar(500),
    precioMax varchar(20),
    precioMin varchar(20),
    administradorId int not null,
    superficie varchar(30),
    textoAgendaTrabajo varchar(200),
    points tinyint default 0,
    creationDate datetime not null
);

DROP TABLE IF EXISTS Images;
CREATE TABLE Images (
    Id int not null primary key AUTO_INCREMENT,
    elementId int not null,
    imagePath varchar(512),
    points tinyint default 0,
    creationDate datetime not null
);

INSERT INTO `Elements` (`Id`, `nombre`, `descripcion`, `tipoPropiedadId`, `lat`, `lng`, `fotoFolder`, `telefono`, `correo`, `horario`, `domicilio`, `precioMax`, `precioMin`, `administradorId`, `superficie`, `textoAgendaTrabajo`, `points`, `creationDate`)
VALUES
	(1,'Sabor Vegano','Sabor Vegano',1,20.667807,-103.368882,NULL,'saborvegano.mx.gdl@gmail.com',NULL,NULL,'Av Chapultepec Sur 480, Obrera, 44140 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(2,'Chia fonda vegana','Chia fonda vegana',1,20.680608,-103.353077,NULL,'+52 33 1393 4291',NULL,NULL,'Calle Mezquitán 274, Zona Centro, 44100 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(3,'El taller Vegano','El taller Vegano',1,20.678537,-103.363129,NULL,'3339538793',NULL,NULL,'Juan Manuel 1251, Santa Teresita, 44600 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(4,'El Vegano','El Vegano',1,20.672954,-103.365840,NULL,'3334795111',NULL,NULL,'Calle Colonias 221, Americana, 44160 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(5,'Krishna Vegan','Krishna Vegan',1,20.675581,-103.356778,NULL,'1282 3458',NULL,NULL,'Calle Pedro Moreno 861, Americana, 44160 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(6,'La estación vegana','La estación vegana',1,20.676471,-103.358401,NULL,'044 33 1009 0171',NULL,NULL,'Calle Juan N. Cumplido 14, Zona Centro, 44100 Guadalajara, Jal.',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(7,'La flaca','La flaca',1,20.677000,-103.369638,NULL,'36167704',NULL,NULL,'Av. Miguel Hidalgo y Costilla 1500, Ladrón de Guevara, Lafayette, 44600 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(8,'The vegan grove','The vegan grove',1,20.700273,-103.334736,NULL,'1916 6700',NULL,NULL,'Platón 1449, Independencia, 44379 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(9,'Gani Tacofu','Gani Tacofu',1,20.683010,-103.373147,NULL,'33 1150 6256',NULL,NULL,'Av. de las Américas 332, Ladrón de Guevara, Ladron De Guevara, 44600 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(10,'Lucuma','Lucuma',1,20.677743,-103.363888,NULL,'40406047',NULL,NULL,'Av. Justo Sierra 1741, Villaseñor, 44600 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(11,'La oliva- Vegan Bistro','La oliva- Vegan Bistro',1,20.684819,-103.362109,NULL,'33 1114 9830',NULL,NULL,'Calle Juan Álvarez 1244, Santa Teresita, 44200 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(12,'Ceiba Café','Ceiba Café',1,20.680166,-103.356403,NULL,'',NULL,NULL,'Calle Reforma 802, Zona Centro, 44100 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(13,'Amaranto Restaurante','Amaranto Restaurante',1,20.618006,-103.419314,NULL,'36311311',NULL,NULL,'Av Sierra de Mazamitla 6015, Díaz Ordaz, 45080 Zapopan, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(14,'Alta Fibra','Alta Fibra',1,20.672869,-103.349019,NULL,'31241510',NULL,NULL,'Calle Prisciliano Sánchez 370B, Zona Centro, 44100 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(15,'Burrito Piñata','Burrito Piñata',1,20.673769,-103.369187,NULL,'3616 4820',NULL,NULL,'Calle Manuel López Cotilla 1542A, Lafayette, 44160 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(16,'Carlota Guacamoles foodtruck','Carlota Guacamoles foodtruck',1,20.634260,-103.414328,NULL,'332 003 2201',NULL,NULL,'Av. Adolfo López Mateos Sur 4300A, La Calma, 45070 Zapopan, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(17,'Chintamani-deli','Chintamani-deli',1,20.675577,-103.357292,NULL,'3335 0432',NULL,NULL,'Calle Pedro Moreno 891, Americana, 44160 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(18,'Delhi 6','Delhi 6',1,20.650422,-103.420073,NULL,'Av Moctezuma 4844, Mirador del',NULL,NULL,'Av Moctezuma 4844, Mirador del Sol, 45054 Zapopan, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(19,'Dharma Food Truck','Dharma Food Truck',1,20.561755,-103.461700,NULL,'333 138 8071',NULL,NULL,'López Mateos sur 3500, 45640 San Agustín, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(20,'El jardin','El jardin',1,20.670461,-103.360232,NULL,'3825 6885',NULL,NULL,'Av. de la Paz 1558, Americana, 44160 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(21,'Falafel','Falafel',1,20.673265,-103.370621,NULL,'3615 7237',NULL,NULL,'Calle Marsella 164, Lafayette, 44160 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(22,'Kitzen Restaurante Vegetariano y vegano','Kitzen Restaurante Vegetariano y vegano',1,20.676034,-103.350472,NULL,'36135425',NULL,NULL,'Calle Donato Guerra 23, Zona Centro, 44100 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(23,'La caravana vegetariana','La caravana vegetariana',1,20.702196,-103.378934,NULL,'3956 0770',NULL,NULL,'Av Providencia 2469, Providencia, 44630 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(24,'La fresca Restaurante','La fresca Restaurante',1,20.701231,-103.377056,NULL,'',NULL,NULL,'São Paulo 2364, Providencia, 44630 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(25,'Naturalmente Verde','Naturalmente Verde',1,20.675482,-103.344945,NULL,'52 33 3458 5511',NULL,NULL,'Calle Degollado 84, Zona Centro, 44100 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(26,'Nutrición Raquelito','Nutrición Raquelito',1,20.699069,-103.342734,NULL,'',NULL,NULL,'Calle Paseo de los Filósofos 1521, Colinas de La Normal, 44270 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(27,'Poutineria','Poutineria',1,20.682049,-103.368694,NULL,'1625 4920',NULL,NULL,'1, Calle Joaquín Angulo 1473, Santa Teresita, 44600 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(28,'Restaurante Villa Madrid','Restaurante Villa Madrid',1,20.674184,-103.351595,NULL,'52 33 1592 5703',NULL,NULL,'Calle Manuel López Cotilla 553, Zona Centro, 44100 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(29,'Restaurante Govinda','Restaurante Govinda',1,20.675302,-103.373399,NULL,'36153127',NULL,NULL,'Calle Pedro Moreno 1791, Lafayette, 44160 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(30,'Rakará','Rakará',1,20.675002,-103.366217,NULL,'52 33 2015 6793',NULL,NULL,'Calle Colonias 84, Americana, 44160 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(31,'Rincón Natural','Rincón Natural',1,20.627233,-103.424469,NULL,'3632 5634',NULL,NULL,'Av. Carnero 5629, Arboledas, 45070 Zapopan, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(32,'Tea Recs Salón de té','Tea Recs Salón de té',1,20.679376,-103.371402,NULL,'3615 2362',NULL,NULL,'Av. México 2136, Santa Teresita, 44600 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(33,'Tia Ofe Pozole Vegetariano','Tia Ofe Pozole Vegetariano',1,20.679597,-103.359213,NULL,'36143680',NULL,NULL,'Av Enrique Díaz de León Nte 220, Zona Centro, 44100 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(34,'Tia Ofe Pozole Vegetariano','Tia Ofe Pozole Vegetariano',1,20.673105,-103.359507,NULL,'36143680',NULL,NULL,'Av Enrique Díaz de León Sur 220, Americana, 44160 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(35,'Zanahoria Vegetariano','Zanahoria Vegetariano',1,20.683843,-103.373563,NULL,'52 33 4526 1543',NULL,NULL,'Av. de las Américas, Ladron De Guevara, Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(36,'Vrindavan Deli','Vrindavan Deli',1,20.676268,-103.380204,NULL,'3331 1553',NULL,NULL,'Calle Pablo Villaseñor 25, Arcos Vallarta, 44130 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(37,'Veggi Comida','Veggi Comida',1,20.657612,-103.318773,NULL,'52 33 3334 1485',NULL,NULL,'Lindavista 1750, La Loma, 44800 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(38,'Vegetariano el buen sabor','Vegetariano el buen sabor',1,20.675660,-103.373243,NULL,'',NULL,NULL,'Calle Morelos 1889, Lafayette, 44160 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(39,'Silvester','Silvester',1,20.667770,-103.366913,NULL,'01 33 1984 7271',NULL,NULL,'Calle Vidrio 1888, Americana, 44160 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(40,'La Flor de Lotto','La Flor de Lotto',1,20.672176,-103.357990,NULL,'3333793467',NULL,NULL,'Calle Miguel Blanco 1413A, Americana, 44160 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(41,'El rincon del mate','El rincon del mate',1,20.672009,-103.358074,NULL,'3338255408',NULL,NULL,'Calle Miguel Blanco 1419, Americana, 44160 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(42,'Ma Shoushou Patisserie','Ma Shoushou Patisserie',1,20.689112,-103.332101,NULL,'15520536606',NULL,NULL,'Calle Los Alpes 962, Independencia Oriente, 44340 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(43,'Organic Now','Organic Now',1,20.668138,-103.369341,NULL,'',NULL,NULL,'Av Chapultepec Sur 480, Obrera, 44140 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(44,'vivero huerto urbano','vivero huerto urbano',2,20.709445,-103.390442,NULL,'33 3123 1132',NULL,NULL,'Av. Patria 1000, Colinas de San Javier, 44660 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(45,'Bosque los Colomos','Bosque los Colomos',2,20.703829,-103.387775,NULL,'',NULL,NULL,'Calle El Chaco 3200, Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(46,'Parque Agroecológico Zapopan','Parque Agroecológico Zapopan',2,20.725661,-103.411352,NULL,'',NULL,NULL,'Santa Laura, 45140 Zapopan, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(47,'Huerto del parque Agua Azul','Huerto del parque Agua Azul',2,20.660739,-103.349240,NULL,'',NULL,NULL,'Calz Independencia Sur 973, Mexicaltzingo, 44790 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(48,'Azoteas comestibles','Azoteas comestibles',2,20.667303,-103.388729,NULL,'3318594182',NULL,NULL,'Firmamento 598, Jardines del Bosque, 44520 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(49,'Cosechame','Cosechame',2,20.665954,-103.367777,NULL,'3312831752',NULL,NULL,'Calle Francia 1933, Moderna, 44190 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(50,'Ecohuerto MX','Ecohuerto MX',2,20.720139,-103.378321,NULL,'31655361',NULL,NULL,'4 96, Ecológica Seattle, 45150 Zapopan, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(51,'Semillas eterno','Semillas eterno',2,20.659860,-103.378221,NULL,'',NULL,NULL,'Mango 1421, Del Fresno, 44909 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(52,'Casa Floresta','Casa Floresta',2,20.650660,-103.416770,NULL,'3318110049',NULL,NULL,'Av Moctezuma 464, Jardines del Sol, 45050 Zapopan, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(53,'Huero urbano ITESO','Huero urbano ITESO',2,20.605061,-103.415554,NULL,'',NULL,NULL,'Independencia 1000, Santa María Tequepexpan, 45609 San Pedro Tlaquepaque, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(54,'Huerto CUCBA','Huerto CUCBA',2,20.745921,-103.512851,NULL,'',NULL,NULL,'Camino Ramón Padilla Sánchez 2100, Nextipac, 44600 Zapopan, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(55,'Huerto agroecológico universitario','Huerto agroecológico universitario',2,20.689024,-103.365586,NULL,'',NULL,NULL,'Calle Jesús García 1542, Villaseñor, 44600 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(56,'Azoteas verdes','Azoteas verdes',2,20.745613,-103.357817,NULL,'',NULL,NULL,'Calle Paseo de los Manzanos 5, Lomas de Tabachines, 45185 Zapopan, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(57,'Cursos de lombricultura','Cursos de lombricultura',2,20.711474,-103.392794,NULL,'',NULL,NULL,'Emiliano Zapata 738, Santa Fe, 45168 Zapopan, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(58,'Edén orgánico','Edén orgánico',2,20.659559,-103.415222,NULL,'',NULL,NULL,'Av de Los Leones 75, Tepeyac Casino, Prados Tepeyac, 45047 Zapopan, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(59,'Los guayabos','Los guayabos',2,20.776696,-103.413788,NULL,'',NULL,NULL,'Dr. Angel Leaño 4001, Colinas de Tesistan, 45134 Nuevo México, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(60,'Colectivo huertos CUSCH','Colectivo huertos CUSCH',2,20.694324,-103.349904,NULL,'',NULL,NULL,'Calle Guanajuato 1045, Artesanos, 44200 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(61,'Mr. Tofu','Mr. Tofu',4,20.710061,-103.411067,NULL,'Plaza Unicenter local #5 Patri',NULL,NULL,'Real de Acueducto, Puerta de Hierro, 45116 Zapopan, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(62,'Abastos Gourmet','Abastos Gourmet',4,20.668467,-103.424209,NULL,'Av. Patria No. 543, Zapopan',NULL,NULL,'Av. Patria 543, Lomas del Seminario, 45038 Zapopan, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(63,'Amor Orgánico y natural','Amor Orgánico y natural',4,20.662765,-103.401206,NULL,'De Las Rosas 559, Chapalita Or',NULL,NULL,'Av de Las Rosas 559, Chapalita Oriente, 45040 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(64,'Bocados de Vida','Bocados de Vida',4,20.623380,-103.415533,NULL,'Av. Sierra de Mazamitla 5644, ',NULL,NULL,'Av Sierra de Mazamitla 5644, Las Águilas, 45080 Zapopan, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(65,'B de Bueno','B de Bueno',4,20.664586,-103.389300,NULL,'Calle de la Nebulosa 2981, Jar',NULL,NULL,'Calle de la Nebulosa 2981, Jardines del Bosque, 44520 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(66,'Gaia eco Store','Gaia eco Store',4,20.696919,-103.386889,NULL,'Providencia. 4a seccion 44639,',NULL,NULL,'Providencia 4a. Secc, 44639 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(67,'Itacate Tiendita Itinerante','Itacate Tiendita Itinerante',4,20.695287,-103.370517,NULL,'H. Colegio Militar 1124, Ayunt',NULL,NULL,'H. Colegio Militar 1124, Ayuntamiento, 44620 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(68,'La Manzana','La Manzana',4,20.684726,-103.368431,NULL,'Calle Manuel Acuña 1516, Santa',NULL,NULL,'Calle Manuel Acuña 1516, Santa Teresita, 44600 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(69,'La Gallina Feliz','La Gallina Feliz',4,20.656610,-103.380210,NULL,'Av. Del Mercado 1097, Mercado ',NULL,NULL,'Av. del Mercado 1097, Mercado de Abasto, 44530 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(70,'Madre Selva Tienda Vengana','Madre Selva Tienda Vengana',4,20.667498,-103.368181,NULL,'Calle Vidrio 1985, Americana, ',NULL,NULL,'Calle Vidrio 1985, Americana, 44160 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(71,'Naturalmente','Naturalmente',4,20.663302,-103.401096,NULL,'Av. Ruben Dario 633, Prados Pr',NULL,NULL,'Av. Rubén Darío 633, Prados Providencia, 44670 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(72,'Pan Gabriel, Organic & Gluten Free Bakery','Pan Gabriel, Organic & Gluten Free Bakery',4,20.686376,-103.385460,NULL,'Av. De las rosas 598, chapalit',NULL,NULL,'Av de Las Rosas 598, Chapalita, Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(73,'Patita Polola Providencia','Patita Polola Providencia',4,20.699973,-103.383157,NULL,'Av. Ruben Dario 1539, Providen',NULL,NULL,'Av. Rubén Darío 1539, Providencia 4a. Secc, 44639 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(74,'Patita Polola Americas','Patita Polola Americas',4,20.676388,-103.373626,NULL,'Av. De las americas 36, Ladron',NULL,NULL,'Av. de las Américas 36, Ladrón de Guevara, Lafayette, 44600 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(75,'Prorganica','Prorganica',4,20.617169,-103.451447,NULL,'Santa ana tepatitalán 2463, Sa',NULL,NULL,'A Sta Ana Tepetitlan 2463, El Camichín, 45230 Zapopan, Jal.',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(76,'Punto Sano','Punto Sano',4,20.621388,-103.316901,NULL,'calle plan sexenal 3433, revol',NULL,NULL,'Calle Plan Sexenal 3433, Revolución, 45580 San Pedro Tlaquepaque, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(77,'Purorganiko','Purorganiko',4,20.669188,-103.430014,NULL,'Av L. Van Beethoven 5335, la e',NULL,NULL,'Av. Ludwig Van Beethoven 5335, La Estancia, 45030 Zapopan, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(78,'Pablito Panaderia','Pablito Panaderia',4,20.679405,-103.362821,NULL,'Calle San felipe 1140, Villase',NULL,NULL,'Calle San Felipe 1140, Santa Teresita, 44100 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(79,'Sentido Comun','Sentido Comun',4,20.679696,-103.377570,NULL,'Calle Luis Pérez Verdía 159, L',NULL,NULL,'Calle Luis Pérez Verdía 159, Ladrón de Guevara, Ladron De Guevara, 44600 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(80,'Toyo Foods','Toyo Foods',4,20.678913,-103.370489,NULL,'Av. México 2097, Ladrón de Gue',NULL,NULL,'Av. México 2097, Ladrón de Guevara, Ladron De Guevara, 44600 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(81,'Alfonso cake','Alfonso cake',4,20.693046,-103.306259,NULL,'Ignacio Machaín Nº 1196, zapop',NULL,NULL,'Calle Ignacio Machain 1196, San Martin, 44710 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(82,'Vegan Bageri- Panaderia','Vegan Bageri- Panaderia',4,20.640433,-103.408144,NULL,'Montemorelos 50-A Zapopan, Col',NULL,NULL,'Calle Montemorelos 50, Loma Bonita, 45167 Zapopan, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(83,'Tu localito Ecologico','Tu localito Ecologico',4,20.672826,-103.362569,NULL,'Prisciliano Sanchez 1067, Amer',NULL,NULL,'Calle Prisciliano Sánchez 1067, Americana, 44160 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(84,'La Abeja Reyna','La Abeja Reyna',4,20.688053,-103.384760,NULL,'Jose Maria Heredia 2735, Lomas',NULL,NULL,'Calle José María Heredia 2735, Lomas de Guevara, 44657 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(85,'El Toque de Aaron','El Toque de Aaron',4,20.682767,-103.367125,NULL,'Ramos Milan 388, guadalajara',NULL,NULL,'Calle Gabriel Ramos Millán 388, Santa Teresita, 44600 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(86,'Cardamomo Bazar Creativo','Cardamomo Bazar Creativo',4,20.681881,-103.395560,NULL,'Av. Toltecas 3385-7, guadalaja',NULL,NULL,'Calle Toltecas 3385, Monraz, 44670 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(87,'Tu Tienda Local','Tu Tienda Local',4,20.715517,-103.368327,NULL,'Lago Superior 2260 Zapopan',NULL,NULL,'Calle Lago Superior 2260, Lagos del Country, 45170 Zapopan, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(88,'Camilo Brew','Camilo Brew',4,20.682767,-103.367125,NULL,'Ramos Milan 388, guadalajara',NULL,NULL,'Calle Gabriel Ramos Millán 388, Santa Teresita, 44600 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(89,'Ohana','Ohana',4,20.639424,-103.316195,NULL,'Calle Independencia 332, Centr',NULL,NULL,'Calle Independencia 332, Centro, 45500 San Pedro Tlaquepaque, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(90,'Organic Now','Organic Now',4,20.668138,-103.369341,NULL,'Local R04 Plaza las Ramblas, A',NULL,NULL,'Av Chapultepec Sur 480, Obrera, 44140 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(91,'Arte Sano del Pan','Arte Sano del Pan',4,20.544666,-103.467513,NULL,'Pedro Loza No. 71b Int. 1, San',NULL,NULL,'Pedro Loza 71a, 45640 San Agustín, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(92,'Mon-Côté','Mon-Côté',4,20.672447,-103.367394,NULL,'Av. de la Paz 1998, Americana,',NULL,NULL,'Av. de la Paz 1998, Americana, 44160 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(93,'Piccolo Gelatoteca','Piccolo Gelatoteca',4,20.675657,-103.362515,NULL,'Calle Pedro Moreno 1164, Ameri',NULL,NULL,'Calle Pedro Moreno 1164, Americana, 44160 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00'),
	(94,'Bazar La Mirilla','Bazar La Mirilla',4,20.700122,-103.382820,NULL,'Parque Silvano Braba en Provid',NULL,NULL,'Silvano Barba Park, Av Ruben Darío 2788, Jardines de Providencia, 44630 Guadalajara, Jal., Mexico',NULL,NULL,1,NULL,NULL,5,'2018-03-01 12:00:00');
/*   */

INSERT INTO Images (elementId, imagePath, creationDate) VALUES
    (280, 'https://redverde.com.mx/images/201709/148/img_20170905035035_normal.png', '2017-09-06T04:50:46.109Z'),
    (280, 'https://redverde.com.mx/images/201709/148/img_20170905041303_normal.png', '2017-09-06T04:57:46.532Z')